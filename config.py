#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#    AkkuVS – A rechargeable battery management system
#    Copyright (C) 2014  Moritz Strohm <ncc1988@posteo.de>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


from django.conf.urls import patterns, include, url
from django.contrib.auth.views import login as django_login
from akkuvs import WebHandler, ApiHandler


weburls = patterns('',
  url(r'loginform', WebHandler.LoginForm, name='web-loginform'),
  url(r'login', WebHandler.Login, name='web-login'),
  url(r'logout', WebHandler.Logout, name='web-logout'),
  url(r'battery/(?P<batteryId>\d+)/', WebHandler.ShowBattery, name="web-show-battery"),
  url(r'search/$', WebHandler.Search, name="web-search"),
  url(r'list/$', WebHandler.ShowList, name="web-show-list"),
  url(r'new/', WebHandler.NewBattery, name="web-new-battery"),
  
  url(r'', WebHandler.Index, name="web-index"),
  )


urls = weburls + patterns('',
  #api frontend:
  url(r'API', ApiHandler.ProcessApiRequest),
  )