# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('akkuvs', '0003_auto_20160216_0857'),
    ]

    operations = [
        migrations.AlterField(
            model_name='battery',
            name='size',
            field=models.ForeignKey(related_name='batteries', to='akkuvs.Size'),
            preserve_default=True,
        ),
    ]
