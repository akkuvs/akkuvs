#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#    AkkuVS – A rechargeable battery management system
#    Copyright (C) 2014  Moritz Strohm <ncc1988@posteo.de>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from datetime import datetime


from django.db import models
from django.db.models import Max

#model classes are transcripted from akkuvs-java


class Group(models.Model):
  name = models.CharField(max_length=128)
  
  def __str__(self):
    return self.name

class Technology(models.Model):
  #techId is omitted
  name = models.CharField(max_length=64, unique=True)
  #TODO: URN for semantic web
  
  def __str__(self):
    return self.name


class Size(models.Model):
  #sizeId is omitted
  name = models.CharField(max_length=64, unique=True)
  #TODO: URN definition for semantic web
  
  def __str__(self):
    return self.name


class Battery(models.Model):
  #no PK definition needed, batteryID is replaced by id
  group = models.ForeignKey(Group, related_name='batteries', null=True, blank=True)
  registration_number = models.CharField(max_length=64, unique=True)
  name = models.CharField(max_length=128)
  technology = models.ForeignKey(Technology, related_name='batteries')
  size = models.ForeignKey(Size, related_name='batteries')
  voltage = models.DecimalField(max_digits=7, decimal_places=2)
  cells = models.IntegerField()
  capacity = models.DecimalField(max_digits=12, decimal_places=2)
  registration_date = models.DateTimeField()
  
  def __str__(self):
    return self.registration_number
  
  def charged(self):
    latest_charge_date = self.charges.aggregate(Max('date'))['date__max']
    latest_discharge_date = self.discharges.aggregate(Max('date'))['date__max']
    if(latest_charge_date != None):
      #one charge is registered
      if(latest_discharge_date != None):
        #at least one charge and one discharge are registered:
        if(latest_charge_date > latest_discharge_date):
          return True
        else:
          return False
      else:
        #there is no discharge registered:
        return True
    else:
      #there is no charge registered:
      #the battery is either discharged or its state is unknown:
      return False


class Charge(models.Model):
  #chargeId = id (automatic PK definition by Django)
  battery = models.ForeignKey(Battery, related_name='charges')
  date = models.DateTimeField()
  current = models.DecimalField(max_digits=10, decimal_places=2, default=0, blank=True)
  measured_capacity = models.DecimalField(max_digits=10, decimal_places=2, default=0, blank=True)
  recharged = models.BooleanField(default=False)
  
  def __str__(self):
      return str(self.battery) + " charged on " + self.date.strftime("%Y-%m-%d %H:%M:%S") + " with " + str(self.current) + " mA"


class Discharge(models.Model):
  #dischargeId is omitted
  battery = models.ForeignKey(Battery, related_name='discharges')
  date = models.DateTimeField()
  
  def __str__(self):
      return str(self.battery) + " discharged on " + self.date.strftime("%Y-%m-%d %H:%M:%S")
  

class StateType(models.Model):
  #stateTypeId is omitted
  name = models.CharField(max_length=64)
  state_class = models.IntegerField(default=0, unique=True) #optional field
  #stateClass can be used to indicate the battery state as numerical value:
  #3 = good, 2 = average, 1 = bad, 0 = unknown
  
  def __str__(self):
    return self.name
  
class State(models.Model):
  #stateId is omitted
  battery = models.ForeignKey(Battery, related_name='states')
  state_type = models.ForeignKey(StateType)
  start_date = models.DateTimeField()


class UsageType(models.Model):
  name = models.CharField(max_length=64)
  usage_class = models.IntegerField(default=0, unique=True)
  #usageClass may as an example be used to indicate the avaiability of a battery:
  # 0 = unknown
  # 1 = short term usage (high avaiability for other usages)
  # 2 = long term usage (low avaiability for other usages)
  # 3 = lifetime usage (can't be used anywhere else)
  
  def __str__(self):
    return self.name
  
class Usage(models.Model):
  #usageId is omitted
  battery = models.ForeignKey(Battery, related_name='usages')
  location = models.CharField(max_length=128)
  usage_type = models.ForeignKey(UsageType)
  start_date = models.DateTimeField()


class FormerBattery(models.Model):
  """
  This is a table extra for batteries that were deleted.
  It might be necessary to store the batterie's information
  after it was deleted.
  """
  group = models.ForeignKey(Group, related_name='former_batteries', null=True, blank=True)
  registration_number = models.CharField(max_length=64, unique=True)
  name = models.CharField(max_length=128)
  technology = models.ForeignKey(Technology)
  size = models.ForeignKey(Size)
  voltage = models.DecimalField(max_digits=7, decimal_places=2)
  cells = models.IntegerField(default=1)
  capacity = models.DecimalField(max_digits=12, decimal_places=2)
  registration_date = models.DateTimeField(default=datetime.fromtimestamp(0))
  deletion_date = models.DateTimeField(datetime.fromtimestamp(0))
  deletion_reason = models.TextField(max_length=4096)

