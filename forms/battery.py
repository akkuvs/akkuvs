
from django.forms import ModelForm

from akkuvs.models import Battery


class NewBatteryForm(ModelForm):
    
    class Meta:
        model = Battery